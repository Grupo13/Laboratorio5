#include<iostream>
#include<bits/stdc++.h>
using namespace std;

struct nodo{
	int nro;
	struct nodo *sgte, *ant;
};
typedef struct nodo *Tlista;

void Mostrar(Tlista lista);
void Insertar_Final(Tlista &lista);
void Burbuja(Tlista &lista);
void Intercambio(Tlista &lista);
void Ord_Insercion(Tlista &lista);

void Seleccion(Tlista &lista);
void menu();

int main(){
	Tlista lista ;
	menu();
	return 0;
}

void menu(){
	Tlista lista=NULL;
	int opcion = 0,n=0;
	system("cls");

	cout << endl;
	do{
	cout << "\t\t\tÉÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ»" << endl;
	cout << "\t\t\tº                MENU                º" << endl;
	cout << "\t\t\tÌÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ¹" << endl;
	cout << "\t\t\tº  1. Insertar numero                º" << endl;
	cout << "\t\t\tº  2. Ordenar por burbuja            º" << endl;
	cout << "\t\t\tº  3. Ordenar por seleccion          º" << endl;
	cout << "\t\t\tº  4. Ordenar por insercion          º" << endl;
	cout << "\t\t\tº  5. Ordenar por intercambio        º" << endl;
	cout << "\t\t\tº  6. Mostrar elementos              º" << endl;
	cout << "\t\t\tº  7. Salir                          º" << endl;
	cout << "\t\t\tÈÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍÍ¼" << endl;
	cout << "\t\t\t\tIngrese una opcion: ";

	cin >> opcion;

	switch (opcion){

	case 1: Insertar_Final(lista);
        	system("cls");
			break;

	case 2:Burbuja(lista);
			system("pause");
			system("cls");
			break;

	case 3:Seleccion(lista);
			system("pause");
			system("cls");
			break;

	case 4: cout<<"ESTA FUNCION NO ESTA TERMINADA"<<endl;
            Ord_Insercion(lista);
			system("pause");
			system("cls");
			break;

	case 5: Intercambio(lista);
			system("pause");
			system("cls");
			break;

	case 6: Mostrar(lista);
			system("pause");
			system("cls");
			break;

	case 7:	system("exit");
			break;
	}
	}while(opcion!=7);
}


void Insertar_Final(Tlista &lista){
	int n;
	Tlista nuevo=NULL;
	Tlista q=lista;
	nuevo=new(struct nodo);
	cout<<"Ingrese el valor del nodo: ";
	cin>>n;
	nuevo->nro=n;
	if(lista==NULL){
		nuevo->sgte=NULL;
		lista=nuevo;
	}
	else{
		while(q->sgte!=NULL){
			q=q->sgte;
		}
		nuevo->sgte=NULL;
		q->sgte=nuevo;
		nuevo->ant=q;
	}
}


void Mostrar(Tlista lista){
	Tlista q=lista;
	while(q->sgte!=NULL){
		cout<<q->nro<<"  ";
		q=q->sgte;
	}
	cout<<q->nro<<"  ";
	cout<<endl;
}


void Burbuja(Tlista &lista){
	Tlista q=lista;
	int n;
	if(q->sgte==NULL && q->ant==NULL){

		cout<<"Solo hay un  elemento\n";
	}else{
		while(q->sgte!=NULL){
			Tlista sgte = q->sgte;
			while(sgte != NULL){
				if(q->nro > sgte->nro){
					n = q->nro;
					q->nro = sgte->nro;
					sgte->nro = n;
			    }
			    sgte = sgte->sgte;
			}
			q = q->sgte;
			sgte = q->sgte;
		}


	}
	cout<<endl<<"Se ha ordenado por el metodo de BURBUJA"<<endl<<endl;
}

void Intercambio(Tlista &lista){
	Tlista q=lista,p=lista;
	int aux;
	while(p->sgte!=NULL){

	while(q->sgte!=NULL){
	q=q->sgte;
		if(p->nro>=q->nro){
			aux=p->nro;
			p->nro=q->nro;
			q->nro=aux;
		}

		}
	p=p->sgte;
	if(q->sgte==NULL){
		while(q!=p){
			q=q->ant;


	}
}

}
cout<<endl<<"Se ha ordenado por el metodo de INTERCAMBIO"<<endl<<endl;
}


void Ord_Insercion(Tlista &lista){
	Tlista q=lista;
	Tlista nuevo=NULL;
	bool pac=false;
	nuevo=new(struct nodo);
	cout<<"Ingrese elemento: ";
	cin>>nuevo->nro;
	if(lista==NULL){
		nuevo->sgte=NULL;
		lista=nuevo;
	}
	else{
		if(lista->sgte==NULL){
			if(nuevo->nro>q->nro){
				q->sgte=nuevo;
				nuevo->ant=q;
				nuevo->sgte=NULL;
			}

			if(nuevo->nro<q->nro){
				nuevo->sgte=q->ant;
				q->ant=nuevo;
				lista=q;
			}
		}
		else{
			if(q->nro>nuevo->nro){ //Si es menor que la cabezera
				nuevo->sgte=q;
				q->ant=nuevo;
				lista=nuevo;
			}
			else{   // Si es mayor que la cabezera
				while(q->nro<nuevo->nro && pac==false){ //Hace que se situe en el nodo que posea el numero inmediatamente despues del que ingresamos
					q=q->sgte;
					if(q->sgte==NULL){
						pac=true;
					}
				}
				if(q->sgte==NULL){
					q->sgte=nuevo;
					nuevo->ant=q;
					nuevo->sgte=NULL;
				}
				else{
					q=q->ant;
					nuevo->sgte=q->sgte;
					q->sgte->ant=nuevo;
					q->sgte=nuevo;
					nuevo->ant=q;
				}

			}
		}
	}

}


void Seleccion(Tlista &lista){
	int n=0;

	Tlista p=lista,q=lista,t=lista;
	while(p->sgte!=NULL){
int	m=9999999;
		q=p->sgte;
		t=p;

        if(q->sgte != NULL){
            while(q->sgte != NULL)
            {
                if(m>q->nro){
                    m=q->nro;
                }
                q=q->sgte;

            }
            if(m>q->nro){
                m=q->nro;
            }
		 }
		 else{
             if(m>p->nro)
                {
                m=p->nro;
                }
              if(m>q->nro)
                {
                m=q->nro;
                }

        }

        while (t->nro != m){
          t=t->sgte;
        }

        if(m<p->nro){
            n=p->nro ;
            p->nro=m;
            t->nro=n;

        }
        p=p->sgte;

	}
    cout<<endl<<"Se ha ordenado por el metodo de SELECCION"<<endl<<endl;
}